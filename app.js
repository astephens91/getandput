const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Roses are red\n Alec is my name\n Myles Turner should take five threes a game ');

});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);

});



